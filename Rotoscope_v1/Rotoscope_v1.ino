/*
   This sketch can be used as a template to create your own dual core application.
   You can either pot code in codeForTask1, codeForTask2, or loop. Make sure, you
   place a delay(1000) in all unused tasks
*/
#include <Arduino.h>

#include <Encoder.h>
#include <ESP_FlexyStepper.h>
const int MOTOR_STEP_PIN = 25;
const int MOTOR_DIRECTION_PIN = 33;
ESP_FlexyStepper stepper;
String readString;

#define ENC_1 34
#define ENC_2 35
Encoder enc(ENC_1, ENC_2);
long lastEncoderValue = 0;


TaskHandle_t Task1, Task2;
SemaphoreHandle_t baton;


void  codeForTask1( void * parameter )
{
  for (;;) {
    while (Serial.available()) {
      delay(3);
      char c =  Serial.read();
      readString += c;
    }
    readString.trim();
    if (readString.length() > 0) {
      Serial.println (readString);
      int index = readString.indexOf(":");
      int dir = (readString.substring(0, index)).toInt();
      int closeDelay = (readString.substring(index + 1)).toInt();

      if (dir == 1) {

        stepper.moveRelativeInSteps(-closeDelay);
        Serial.println("CW");

      } else if (dir == 2) {
        stepper.moveRelativeInSteps(closeDelay);
        Serial.println("CCW");
      }
      readString = "";
    }
    delay(10);
  }
}

void  codeForTask2( void * parameter )
{
  for (;;) {
    delay(10);
  }
}

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(115200);

  baton = xSemaphoreCreateMutex();

  stepper.connectToPins(MOTOR_STEP_PIN, MOTOR_DIRECTION_PIN);
  stepper.setSpeedInStepsPerSecond(1000);
  stepper.setAccelerationInStepsPerSecondPerSecond(800);

  xTaskCreatePinnedToCore(
    codeForTask1,      /* Task function. */
    "Listen Serial & motor control",   /* name of task. */
    10000,             /* Stack size of task */
    NULL,              /* parameter of the task */
    2,                 /* priority of the task */
    &Task1, /* Task handle to keep track of created task */
    0);                /* pin task to core 0 */



  delay(500);  // needed to start-up task1

  xTaskCreatePinnedToCore(
    codeForTask2,      /* Task function. */
    "Read rotary",   /* name of task. */
    10000,             /* Stack size of task */
    NULL,              /* parameter of the task */
    2,                 /* priority of the task */
    &Task2, /* Task handle to keep track of created task */
    1);                /* pin task to core 0 */

}


void loop() {
  long encoderValue = enc.read();

  Serial.println(encoderValue);

}
