#include <Arduino.h>
#include "BasicStepperDriver.h"
#define MOTOR_STEPS 50
#define RPM 7000
#define MICROSTEPS 1

#define DIR 33
#define STEP 25
BasicStepperDriver stepper(MOTOR_STEPS, DIR, STEP);
String readString;
int closeDelay = 100;


int encoderPin1 = 34;
int encoderPin2 = 35;
volatile int lastEncoded = 0;
volatile long encoderValue = 0;
long lastencoderValue;
long lastEncoderValue = 0;
int lastMSB = 0;
int lastLSB = 0;


TaskHandle_t Task1;


void  codeForTask1( void * parameter )
{
  for (;;) {
    while (Serial.available()) {
      delay(3);
      char c =  Serial.read();
      readString += c;
    }
    readString.trim();
    if (readString.length() > 0) {
      // Serial.println (readString);
      long index = readString.indexOf(":");
      long dir = (readString.substring(0, index)).toInt();
      long closeDelay = (readString.substring(index + 1)).toInt();

      if (dir == 1) {
        stepper.move(-closeDelay * MICROSTEPS);
      } else if (dir == 2) {
        stepper.move(closeDelay * MICROSTEPS);
      }
      readString = "";

    }
    delay(10);
  }
}

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(115200);
  stepper.begin(RPM, MICROSTEPS);
  stepper.setSpeedProfile(stepper.LINEAR_SPEED, 10000, 10000);



  pinMode(encoderPin1, INPUT);
  pinMode(encoderPin2, INPUT);
  digitalWrite(encoderPin1, HIGH); //turn pullup resistor on
  digitalWrite(encoderPin2, HIGH); //turn pullup resistor on

  attachInterrupt(0, updateEncoder, CHANGE);
  attachInterrupt(1, updateEncoder, CHANGE);

  xTaskCreatePinnedToCore(
    codeForTask1,      /* Task function. */
    "Listen Serial & motor control",   /* name of task. */
    10000,             /* Stack size of task */
    NULL,              /* parameter of the task */
    2,                 /* priority of the task */
    &Task1, /* Task handle to keep track of created task */
    0);                /* pin task to core 0 */

  //delay(100);  // needed to start-up task1
}


void loop() {

  Serial.println(encoderValue/4);
}

void updateEncoder() {
  int MSB = digitalRead(encoderPin1); //MSB = most significant bit
  int LSB = digitalRead(encoderPin2); //LSB = least significant bit
  int encoded = (MSB << 1) | LSB; //converting the 2 pin value to single
  //number
  int sum = (lastEncoded << 2) | encoded; //adding it to the previous
  //encoded value
  if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
    encoderValue ++;
  if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
    encoderValue --;
  lastEncoded = encoded; //store this value for next time
}
